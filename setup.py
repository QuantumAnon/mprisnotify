from setuptools import setup

setup(name='mprisnotify',
      version='0.0.1',
      description='Presents now playing notifications for MPRIS capable music players',
      url='',
      author='Gautam Chaudhuri',
      author_email='',
      license='MIT',
      packages=['mprisnotify'],
      zip_safe=False)
